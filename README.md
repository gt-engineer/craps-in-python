# Craps

A text based craps game written in Python and Matlab

This program simulates a simple game of craps.

A simple game of craps is defined as playing either the pass or don't pass line.

A simple game of craps does not include playing any other bet on the table.

I am working to include the code to allow a pass/don't pass line better to take/lay odds.

Also, I know there is bloat/inefficiencies in the code now.  Once everything is working like I want it to, I'll work on optimizing the code.
